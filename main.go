package main

import (
    "log"
    "net/http"

    "github.com/gorilla/mux"
)

func HomeHandler(rw http.ResponseWriter, r *http.Request) {
    http.ServeFile(rw, r, "index.html")
}

func main() {
    r := mux.NewRouter()

    // cssHandler := http.FileServer(http.Dir("./css/"))
    // imagesHandler := http.FileServer(http.Dir("./images/"))

    // http.Handle("/css/", http.StripPrefix("/css/", cssHandler))
    // http.Handle("/css/", http.StripPrefix("/css/", http.FileServer(http.Dir("./css"))))
    http.Handle("/css/", http.StripPrefix("/css/", http.FileServer(http.Dir("./css"))))

    // log.Fatal(http.ListenAndServe(":8080", nil))
    // http.Handle("/images/", http.StripPrefix("/images/", imagesHandler))
    r.HandleFunc("/", HomeHandler)
    http.Handle("/", r)

    log.Println("Server running on :8080")
    log.Fatal(http.ListenAndServe(":8080", nil))
    // log.Fatal(http.ListenAndServe(":8080", r))
}
